import "phaser"
import {MachineConfig} from 'xstate'

const game = new Phaser.Game({
    backgroundColor: 0x00a2e8
})


class MainScene extends Phaser.Scene {
   preload(){
        this.load.image('ship1','assets/sprites/BattleShip.png')
   }
    create(){
        this.add.existing(new Ship(this, this.game.renderer.width/2, this.game.renderer.height/2, 'ship1'))
    }
}

class Ship extends Phaser.GameObjects.Sprite {

}


game.scene.add('main', MainScene)
game.scene.run('main')
